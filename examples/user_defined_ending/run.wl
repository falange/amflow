(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "FIRE+LiteRed"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = box1;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p4^2 -> 0, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p4)^2};
AMFlowInfo["Numeric"] = {s -> 100, t -> -1};
AMFlowInfo["NThread"] = 4;


(*reduce target integrals to preferred master integrals*)
target = {j[box1,2,0,1,0], j[box1,-2,1,1,2], j[box1,1,2,2,1]};
preferred = {};
{masters, rules} = BlackBoxReduce[target, preferred];


(*automatically define the list of eps-values, truncated order and working precision for following numerical evaluations*)
precision = 50;
epsorder = 5;
{epslist, xorder, wkpre} = GenerateNumericalConfig[precision, epsorder];
SetAMFOptions["XOrder" -> xorder, "WorkingPre" -> wkpre];


(*SetAMFOptions["EndingScheme" \[Rule] {keyword1, keyword2,...}], default (built-in) keywords: {"Tradition", "Cutkosky", "SingleMass"}*)
(*each keyword corresponds to an ending scheme, which will define a type of ending boundary integrals*)
(*"Tradition": the ending boundary integrals are either single-mass vacuum integrals or pure phase-space volumes*)
(*"Cutkosky": pure phase-space volumes will be transformed to loop integrals and evaluated using Cutkosky rules*)
(*"SingleMass": single-mass vacuum integrals will be transformed to p-integrals and evaluated*)
(*so by default, we won't need to input any boundary integrals. The ending systems are trivial (0-loop).*)
(*here we first try the traditional scheme which will end up with single-mass vacuum integrals*)
SetAMFOptions["EndingScheme" -> {"Tradition"}];


(*define a working directory*)
$AMFDirectory = CacheDirectory["test1"]


(*set up all differential systems*)
sysids = AMFSystemsSetup[masters]


(*check the tree structure of systems*)
amftree = Join@@(AMFSystemsTree/@sysids)


(*check the ending systems*)
ending = Join@@(AMFSystemsEnding/@sysids)


(*read from files the information of desired boundary integrals for system 3*)
(*it can be straightforwardly seen that this integral is a one-loop tadpole, whose result is -Gamma[-1+eps]*)
AMFSystemRead[3, "Config"]
AMFSystemRead[3, "Preferred"]


(*write the results of boundary integrals to system 3*)
AMFSystemWrite[3, "Solution", {j[box1,1,0,0,0] -> -Gamma[-1+epslist]}]


(*also for system 4*)
AMFSystemRead[4, "Config"]
AMFSystemRead[4, "Preferred"]
AMFSystemWrite[4, "Solution", {j[box1,1,0,0,0] -> -Gamma[-1+epslist]}]


(*next we can evaluate the systems to obtain numerical results*)
sol = AMFSystemsSolution[sysids, epslist];


(*fit the expansion of master integrals and obtain final results for target integrals*)
leading = -2;
values = Transpose@MapThread[(Values[rules]/.eps->#1) . #2&, {epslist, Transpose[masters/.sol]}];
exp = FitEps[epslist, #, leading]&/@values;
final = Thread[Keys[rules] -> Chop@N[Normal@Series[exp, {eps, 0, leading+epsorder}], precision]];


(*save the results*)
Put[final, FileNameJoin[{current, "final_Tradition"}]];


(*next we try to define a new ending scheme named "usr", which will have an additional ending at one-loop massless bubble integrals*)
(*two functions should be defined for this scheme: AMFSystemEndingQ[masters_, "usr"] and AMFSystemSetupMaster[sysid_, masters_, "usr"]*)
SetAMFOptions["EndingScheme" -> {"usr"}];


(*AMFSystemEndingQ[masters, keyword] judges whether the system should be an ending system. True means an ending and False means not an ending.*)
(*AMFSystemEndingQ[masters, {keyword1, keyword2,...}] judges the system by the list of schemes specified by the keywords in sequence and returns False if any one of the scheme yields False*)
(*built-in keywords: "Tradition", "Cutkosky", "SingleMass"*)
(*if the number of propagators is 2, then end, else use the default ending schemes to judge*)
AMFSystemEndingQ[masters_, "usr"]:=If[Length[GetTopPosition[masters]]===2, True, AMFSystemEndingQ[masters, {"Tradition", "Cutkosky", "SingleMass"}]];


(*AMFSystemSetupMaster[sysid, masters, keyword] sets up system with the ending scheme specified by keyword, should only work when AMFSystemEndingQ[masters, keyword] yields False*)
(*built-in keywords: "Tradition", "Cutkosky", "SingleMass"*)
(*because we aim to introduce an additional ending, so no further coding is needed, we just use the default schemes*)
AMFSystemSetupMaster[sysid_, masters_, "usr"]:=AMFSystemSetupMaster[sysid, masters, {"Tradition", "Cutkosky", "SingleMass"}];


(*define a working directory*)
$AMFDirectory = CacheDirectory["test2"]


(*set up all differential systems*)
sysids = AMFSystemsSetup[masters]


(*check the tree structure of systems*)
amftree = Join@@(AMFSystemsTree/@sysids)


(*check the ending systems*)
ending = Join@@(AMFSystemsEnding/@sysids)


(*read from files the information of desired boundary integrals for system 2*)
(*it can be straightforwardly seen that this integral is a one-loop massless bubble, whose result is Gamma[1-eps]^2Gamma[eps]/Gamma[2-2eps]*)
AMFSystemRead[2, "Config"]
AMFSystemRead[2, "Preferred"]


(*write the results of boundary integrals to system 2*)
AMFSystemWrite[2, "Solution", {j[box1,1,0,1,0] -> Gamma[1-eps]^2Gamma[eps]/Gamma[2-2eps]/.eps->epslist}]


(*read from files the information of desired boundary integrals for system 3*)
(*this is the so-called 0-loop integral, so we don't need to input anything*)
AMFSystemRead[3, "Config"]
AMFSystemRead[3, "Preferred"]


(*next we can evaluate the systems to obtain numerical results*)
sol = AMFSystemsSolution[sysids, epslist];


(*fit the expansion of target integrals*)
leading = -2;
values = Transpose@MapThread[(Values[rules]/.eps->#1) . #2&, {epslist, Transpose[masters/.sol]}];
exp = FitEps[epslist, #, leading]&/@values;
final2 = Thread[Keys[rules] -> Chop@N[Normal@Series[exp, {eps, 0, leading+epsorder}], precision]];


(*save the results*)
Put[final2, FileNameJoin[{current, "final_usr"}]];


Quit[];
