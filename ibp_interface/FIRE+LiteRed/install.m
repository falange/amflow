(* ::Package:: *)

(*FIRE installation directory*)

(*For Unix:*)
$FIREInstallationDirectory = "/mnt/zfsusers/xiao6/research/software/fire/FIRE6";

(*For Windows with WSL:*)
(*$FIREInstallationDirectory = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fire\\FIRE6";*)


(*FIRE version*)
$FIREVersion = 6;
