(* ::Package:: *)

(*For Unix:*)


(*path to FiniteFlow.m*)
$FFPath = "/mnt/zfsusers/xiao6/research/mmapkg/external/finiteflow";
(*path to fflowmlink.so (in Linux) or fflowmlink.dylib (in MacOS)*)
$FFLibraryPath = "/mnt/zfsusers/xiao6/research/local/ffmath";
(*path to LiteIBP.m*)
$LiteIBPPath = "/mnt/zfsusers/xiao6/research/mmapkg/external/finiteflow-mathtools-master/packages";
(*path to LiteRed.m*)
$LiteRedPath = "/mnt/zfsusers/xiao6/research/mmapkg/external/LiteRed";


(*For Windows with WSL:*)



(*(*path to FiniteFlow.m*)
$FFPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fflow\\finiteflow-master\\mathlink";
(*path to fflowmlink.so (in Linux) or fflowmlink.dylib (in MacOS)*)
$FFLibraryPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fflow\\finiteflow-master";
(*path to LiteIBP.m*)
$LiteIBPPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\fflow\\finiteflow-mathtools-master\\packages";
(*path to LiteRed.m*)
$LiteRedPath = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\LiteRed\\Setup";
*)
