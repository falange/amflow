(* ::Package:: *)

(*For Unix:*)


(*kira executable, version 2.2 or later*)
$KiraExecutable = "/mnt/zfsusers/xiao6/research/local/kira/kira-2.3";
(*fermat executable*)
$FermatExecutable = "/mnt/zfsusers/xiao6/research/local/ferl6/fer64";


(*For Window with WSL*)



(*(*kira executable, version 2.2 or later*)
$KiraExecutable = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\kira\\kira-2.3";
(*fermat executable*)
$FermatExecutable = "\\\\wsl.localhost\\Ubuntu\\usr\\local\\src\\ferl6\\fer64";
*)
